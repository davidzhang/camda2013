#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import operator
import pylab as pl

import config


species = "human_invitro"
folder = config.human_invitro_folder
hour = ''
level = ''

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=4,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def preprocess():
    # Get control expression data
    control_fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
    control_df = pd.read_csv(control_fpath, header=0, index_col=0)
    del control_df['DILI_Class']
    print "Control: %s" % str(control_df.shape)
    targets = [0] * control_df.shape[0]

    exp_fpath = os.path.join(folder, "expr_%s_%dhr.csv" % (level, hour))
    exp_df = pd.read_csv(exp_fpath, header=0, index_col=0)
    
    # Get experiment expression data with less damage
    less_exp_df = exp_df[exp_df['DILI_Class']==1]
    del less_exp_df['DILI_Class']
    targets.extend([1] * less_exp_df.shape[0])
    print "less damage: %s" % str(less_exp_df.shape)

    # Get experiment expression data with most damage
    most_exp_df = exp_df[exp_df['DILI_Class']==2]
    del most_exp_df['DILI_Class']
    targets.extend([1] * most_exp_df.shape[0])
    print "Most damage: %s" % str(most_exp_df.shape)
    
    targets = np.array(targets)

    data_df = pd.concat([control_df, less_exp_df, most_exp_df])
    print "Data: %s" % str(data_df.shape)
    print "Targets: %s" % str(targets.shape)
    return data_df, targets


def rf_fs(data_df, targets):
    genes = data_df.columns
    data = np.array(data_df)
    model = randomforest(data, targets, tree_num=1000)
    index_imp = {}
    for ind, v in enumerate(model.feature_importances_):
        index_imp[ind] = v
    sorted_tuple = sorted(index_imp.iteritems(), key=operator.itemgetter(1), reverse=True)

    fs_folder = os.path.join("../fs", species)
    if not os.path.exists(fs_folder):
        os.makedirs(fs_folder)

    with open(os.path.join(fs_folder, "%s_%s" % (level, hour)), "w") as fh:
        for ind, imp in sorted_tuple:
            fh.write("%s,%f\n" % (genes[ind], imp))


def main():
    hours = [8, 24]
    dose_levels = ['Middle', 'High']

    for lv in dose_levels:
        for hr in hours:
            global level
            global hour
            level = lv
            hour = hr
            data_df, targets = preprocess()
            rf_fs(data_df, targets)


if __name__ == "__main__":
    main()

