#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import pylab as pl

import config


name = "rat_invitro"

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=4,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def preprocess():
    targets = []
    data = []

    folder = config.rat_invitro_folder
    hours = [2, 8, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    print "Control: %d" % len(targets)

    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)

    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    print "Data: %s" % str(data.shape)
    print "Target: %s" % str(targets.shape)
    return data, targets

def classify_cv_roc(data, targets):
    kf = KFold(data.shape[0], k=5, shuffle=True)
    
    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    all_tpr = []

    count = 1
    for train_index, test_index in kf:
        model = randomforest(data[train_index], targets[train_index], tree_num=1000)
        probas_ = model.predict_proba(data[test_index])
        fpr, tpr, thresholds = roc_curve(targets[test_index], probas_[:, 1])
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        #pl.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (count, roc_auc))
        count += 1

    mean_tpr /= len(kf)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    pl.plot(mean_fpr, mean_tpr, 'k--', label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)
    #pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('ROC')
    pl.legend(loc="lower right")
    pl.savefig("../plots/%s_roc.pdf" % name)
    pl.show()

def classify_cv_report(data_df, targets):
    data = np.array(data_df)
    kf = KFold(data.shape[0], k=5, shuffle=True)
    for train_index, test_index in kf:
        model = randomforest(data[train_index], targets[train_index], tree_num=1000)
        predictions = model.predict(data[test_index])
        print classification_report(targets[test_index], predictions, target_names=['control', 'exp'])

def main():
    data, targets = preprocess()
    classify_cv_roc(data, targets)

if __name__ == "__main__":
    main()

