from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import operator
import pylab as pl

import config
import util

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=4,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def get_invivo_single_data():
    targets = []
    data = []

    # rat invivo
    folder = config.rat_invivo_single_folder
    hours = [3, 6, 9, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)

    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def get_invitro_data():
    targets = []
    data = []

    # rat invitro
    folder = config.rat_invitro_folder
    hours = [2, 8, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)
    
    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def get_human_data():
    targets = []
    data = []

    # human invitro
    folder = config.human_invitro_folder
    hours = [2, 8, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)
    
    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def rf_fs(data, targets, genes, fname):
    model = randomforest(data, targets, tree_num=1000)
    index_imp = {}
    for ind, v in enumerate(model.feature_importances_):
        index_imp[ind] = v
    sorted_tuple = sorted(index_imp.iteritems(), key=operator.itemgetter(1), reverse=True)

    with open("../fs/%s" % fname, "w") as fh:
        for ind, imp in sorted_tuple:
            fh.write("%s,%f\n" % (genes[ind], imp))

def rat_invitro_fs():
    data, targets = get_invitro_data()
    genes = util.read_gene_names(config.rat_invitro_gene)
    rf_fs(data, targets, genes, "rat_invitro_all")

def rat_invivo_single_fs():
    data, targets = get_invivo_single_data()
    genes = util.read_gene_names(config.rat_invivo_single_gene)
    rf_fs(data, targets, genes, "rat_invivo_single_all")

def human_invitro_fs():
    data, targets = get_human_data()
    genes = util.read_gene_names(config.human_invitro_gene)
    rf_fs(data, targets, genes, "human_invitro_all")


def main():
    #rat_invitro_fs()
    #rat_invivo_single_fs()
    human_invitro_fs()

if __name__ == "__main__":
    main()

