#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import pylab as pl

import config
import util

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=4,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def get_invivo_data():
    targets = []
    data = []

    # rat invivo
    folder = config.rat_invivo_single_folder
    hours = [3, 6, 9, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)
    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def get_invitro_data():
    targets = []
    data = []

    # rat invitro
    folder = config.rat_invitro_folder
    hours = [2, 8, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)
    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def get_human_data():
    targets = []
    data = []

    # human invivo
    folder = config.human_invitro_folder
    hours = [2, 8, 24]
    dose_levels = ['Low', 'Middle', 'High']
    
    # Get control expression data
    for hour in hours:
        fpath = os.path.join(folder, "expr_Control_%shr.csv" % hour)
        with open(fpath) as fh:
            fh.readline()
            for line in fh:
                line = line.strip()
                arr = line.split(",")
                arr.pop(0) #sample id
                dili = arr.pop(-1) #DILI_Class
                targets.append(0)
                data.append(arr)
    
    # Get experiment expression data with damage
    for hour in hours:
        for level in dose_levels:
            fpath = os.path.join(folder, "expr_%s_%shr.csv" % (level, hour))
            with open(fpath) as fh:
                fh.readline()
                for line in fh:
                    line = line.strip()
                    arr = line.split(",")
                    arr.pop(0) #sample id
                    dili = arr.pop(-1) #DILI_Class
                    if dili=='1' or dili=='2':
                        targets.append(1)
                        data.append(arr)
    data = np.array(data, dtype=np.float64)
    targets = np.array(targets)
    return data, targets

def invitro2invivo_roc():
    train_data, train_targets = get_invitro_data()
    test_data, test_targets = get_invivo_data()
    
    tree_num = 1000
    model = randomforest(train_data, train_targets, tree_num=tree_num)
    probas_ = model.predict_proba(test_data)
    fpr, tpr, thresholds = roc_curve(test_targets, probas_[:, 1])
    roc_auc = auc(fpr, tpr)
    pl.plot(fpr, tpr, lw=1, label='AUC = %0.2f)' % roc_auc)
    
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('ROC')
    pl.legend(loc="lower right")
    pl.savefig("../plots/invitro2invivo_%dtree.pdf" % tree_num)
    pl.show()

def invivo2invitro_roc():
    train_data, train_targets = get_invivo_data()
    test_data, test_targets = get_invitro_data()
    
    tree_num = 100
    model = randomforest(train_data, train_targets, tree_num=tree_num)
    probas_ = model.predict_proba(test_data)
    fpr, tpr, thresholds = roc_curve(test_targets, probas_[:, 1])
    roc_auc = auc(fpr, tpr)
    pl.plot(fpr, tpr, lw=1, label='AUC = %0.2f)' % roc_auc)
    
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('ROC')
    pl.legend(loc="lower right")
    pl.savefig("../plots/invivo2invitro_%dtree.pdf" % tree_num)
    pl.show()

def rat_invitro2human_roc():
    rat_data, train_targets = get_invitro_data()
    human_data, test_targets = get_human_data()
    
    rat_genes = util.read_gene_names(config.rat_invitro_gene)
    rat_df = pd.DataFrame(rat_data, columns=rat_genes)

    human_genes = util.read_gene_names(config.human_invitro_gene)
    human_df = pd.DataFrame(human_data, columns=human_genes)

    mapping = util.get_rat_human_mapping()
    rat_probes = []
    human_probes = []
    for probe in mapping:
        rat_probes.append(probe)
        human_probes.append(mapping[probe])

    rat_df = rat_df[rat_probes]
    human_df = human_df[human_probes]

    train_data = np.array(rat_df)
    test_data = np.array(human_df)
    print "Train data: %s" % str(train_data.shape)
    print "Test data: %s" % str(test_data.shape)

    tree_num = 100
    model = randomforest(train_data, train_targets, tree_num=tree_num)
    probas_ = model.predict_proba(test_data)
    fpr, tpr, thresholds = roc_curve(test_targets, probas_[:, 1])
    roc_auc = auc(fpr, tpr)
    pl.plot(fpr, tpr, lw=1, label='AUC = %0.2f)' % roc_auc)
    
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('ROC')
    pl.legend(loc="lower right")
    pl.savefig("../plots/rat_invitro2human_%dtree.pdf" % tree_num)
    pl.show()

def rat_invivo2human_roc():
    rat_data, train_targets = get_invivo_data()
    human_data, test_targets = get_human_data()
    
    rat_genes = util.read_gene_names(config.rat_invivo_single_gene)
    rat_df = pd.DataFrame(rat_data, columns=rat_genes)

    human_genes = util.read_gene_names(config.human_invitro_gene)
    human_df = pd.DataFrame(human_data, columns=human_genes)

    mapping = util.get_rat_human_mapping()
    rat_probes = []
    human_probes = []
    for probe in mapping:
        rat_probes.append(probe)
        human_probes.append(mapping[probe])

    rat_df = rat_df[rat_probes]
    human_df = human_df[human_probes]

    train_data = np.array(rat_df)
    test_data = np.array(human_df)
    print "Train data: %s" % str(train_data.shape)
    print "Test data: %s" % str(test_data.shape)

    tree_num = 100
    model = randomforest(train_data, train_targets, tree_num=tree_num)
    probas_ = model.predict_proba(test_data)
    fpr, tpr, thresholds = roc_curve(test_targets, probas_[:, 1])
    roc_auc = auc(fpr, tpr)
    pl.plot(fpr, tpr, lw=1, label='AUC = %0.2f)' % roc_auc)
    
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('ROC')
    pl.legend(loc="lower right")
    pl.savefig("../plots/rat_invivo2human_%dtree.pdf" % tree_num)
    pl.show()

def main():
    #invitro2invivo_roc()
    #invivo2invitro_roc()
    rat_invitro2human_roc()
    #rat_invivo2human_roc()

if __name__ == "__main__":
    main()
