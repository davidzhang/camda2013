#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import pylab as pl

import config


folder = config.rat_invivo_single_folder
hour = '24hr'
level = 'Low'

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=10,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def svm(data, targets):
    model = SVC(verbose=0, kernel='linear', probability=True)
    model.fit(data, targets)
    return model

def preprocess():
    targets = []
    
    # Get control expression data
    control_fpath = os.path.join(folder, "expr_Control_%s.csv" % hour)
    control_df = pd.read_csv(control_fpath, header=0, index_col=0)
    del control_df['DILI_Class']
    targets.extend([0] * control_df.shape[0])
    print control_df.shape
    
    # Get experiment expression data with less damage
    exp_fpath = os.path.join(folder, "expr_%s_%s.csv" % (level, hour))
    less_exp_df = pd.read_csv(exp_fpath, header=0, index_col=0)
    less_exp_df = less_exp_df[less_exp_df['DILI_Class']==1]
    del less_exp_df['DILI_Class']
    targets.extend([1] * less_exp_df.shape[0])
    print less_exp_df.shape

    # Get experiment expression data with most damage
    most_exp_df = pd.read_csv(exp_fpath, header=0, index_col=0)
    most_exp_df = most_exp_df[most_exp_df['DILI_Class']==2]
    del most_exp_df['DILI_Class']
    targets.extend([1] * most_exp_df.shape[0])
    print most_exp_df.shape

    data_df = pd.concat([control_df, less_exp_df, most_exp_df])
    #data_df = pd.concat([less_exp_df, most_exp_df])
    return data_df, np.array(targets)

def classify_cv_roc(data_df, targets):
    data = np.array(data_df)
    kf = KFold(data.shape[0], k=5, shuffle=True)
    #kf = StratifiedKFold(targets, k=5)
    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    all_tpr = []

    count = 1
    for train_index, test_index in kf:
        model = randomforest(data[train_index], targets[train_index], tree_num=100)
        #model = svm(data[train_index], targets[train_index])
        probas_ = model.predict_proba(data[test_index])
        fpr, tpr, thresholds = roc_curve(targets[test_index], probas_[:, 1])
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        #pl.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (count, roc_auc))
        count += 1

    mean_tpr /= len(kf)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    pl.plot(mean_fpr, mean_tpr, 'k--', label='Mean AUC = %0.2f' % mean_auc, lw=2)
    #pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('Mean ROC Curve')
    pl.legend(loc="lower right")
    pl.show()

def classify_cv_report(data_df, targets):
    data = np.array(data_df)
    kf = KFold(data.shape[0], k=5, shuffle=True)
    for train_index, test_index in kf:
        model = randomforest(data[train_index], targets[train_index], tree_num=100)
        #model = svm(data[train_index], targets[train_index])
        predictions = model.predict(data[test_index])
        print classification_report(targets[test_index], predictions, target_names=['control', 'exp'])


def main():
    data_df, targets = preprocess()
    classify_cv_roc(data_df, targets)
    #classify_cv_report(data_df, targets)


if __name__ == "__main__":
    main()

