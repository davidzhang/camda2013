#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import roc_curve, auc, precision_score, classification_report
import pandas as pd
import numpy as np
from scipy import interp
import os
import pylab as pl

import config


folder = config.rat_invivo_repeat_folder
level = 'High'

def randomforest(data, targets, tree_num=100):
    model = RandomForestClassifier(n_estimators=tree_num,
                                   n_jobs=4,
                                   max_features=data.shape[1]/2+1,
                                   verbose=0,
                                   oob_score=True,
                                   compute_importances=True,
                                   random_state=12345678)
    model.fit(data, targets)
    return model

def run():
    days = [4, 8, 15, 29]

    for day in days:
        targets = []

        # Get control expression data
        control_fpath = os.path.join(folder, "expr_Control_%dday.csv" % day)
        control_df = pd.read_csv(control_fpath, header=0, index_col=0)
        del control_df['DILI_Class']
        targets.extend([0] * control_df.shape[0])
        print control_df.shape
    
        # Get experiment expression data with less damage
        exp_fpath = os.path.join(folder, "expr_%s_%dday.csv" % (level, day))
        exp_df = pd.read_csv(exp_fpath, header=0, index_col=0)
        less_exp_df = exp_df[exp_df['DILI_Class']==1]
        del less_exp_df['DILI_Class']
        targets.extend([1] * less_exp_df.shape[0])
        print less_exp_df.shape

        # Get experiment expression data with most damage
        most_exp_df = exp_df[exp_df['DILI_Class']==2]
        del most_exp_df['DILI_Class']
        targets.extend([1] * most_exp_df.shape[0])
        print most_exp_df.shape
    
        data_df = pd.concat([control_df, less_exp_df, most_exp_df])
    
        data = np.array(data_df)
        targets = np.array(targets)

        kf = KFold(data.shape[0], k=5, shuffle=True)
        mean_tpr = 0.0
        mean_fpr = np.linspace(0, 1, 100)
        all_tpr = []

        count = 1
        for train_index, test_index in kf:
            model = randomforest(data[train_index], targets[train_index], tree_num=100)
            probas_ = model.predict_proba(data[test_index])
            fpr, tpr, thresholds = roc_curve(targets[test_index], probas_[:, 1])
            mean_tpr += interp(mean_fpr, fpr, tpr)
            mean_tpr[0] = 0.0
            roc_auc = auc(fpr, tpr)
            count += 1

        mean_tpr /= len(kf)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        pl.plot(mean_fpr, mean_tpr, label='Mean AUC = %0.2f (%d day)' % (mean_auc, day), lw=2)
    
    pl.xlim([-0.05, 1.05])
    pl.ylim([-0.05, 1.05])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('Mean ROC Curve')
    pl.legend(loc="lower right")
    pl.savefig('../plots/rat_invivo_repeat_%s.pdf' % level)
    pl.show()

def classify_cv_report(data_df, targets):
    data = np.array(data_df)
    kf = KFold(data.shape[0], k=5, shuffle=True)
    for train_index, test_index in kf:
        model = randomforest(data[train_index], targets[train_index], tree_num=100)
        predictions = model.predict(data[test_index])
        print classification_report(targets[test_index], predictions, target_names=['control', 'exp'])

def main():
    run()
    #classify_cv_report(data_df, targets)

if __name__ == "__main__":
    main()
