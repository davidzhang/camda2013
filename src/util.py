import os
import csv
import pandas as pd
import numpy as np

import config


def read_sample_names(fpath):
    names = []
    with open(fpath) as fh:
        for line in fh:
            names.append(line.strip().strip("\""))
    return names

def read_gene_names(fpath):
    names = []
    with open(fpath) as fh:
        for line in fh:
            names.append(line.strip().split(",")[0].strip("\""))
    return names
    

def get_drug_data():
    """Get drug damage information to liver
       Return a dict with drug name as key, and damage type and damage value as 
       second level dict
    """
    drug_label = {}

    with open(config.drug_fpath) as fh:
        fh.readline()
        for line in fh:
            arr = line.strip().split(",")
            # Last column is damage to liver, second column is drug name
            level = arr[-1].strip()
            if len(level) == 0:
                continue
            else:
                name = arr[1].strip()
                number = arr[-2].strip()
                if level.startswith("no"):
                    level = 0
                    number = 0
                elif level.startswith("Less"):
                    level = 1
                    number = int(number)
                elif level.startswith("Most"):
                    level = 2
                    if len(number) == 0:
                        number = 10
                    else:
                        number=int(number)

            drug_label[name] = {}
            drug_label[name]['class'] = level
            drug_label[name]['number'] = number
    return drug_label

def split_metadata():
    """Split gene expression metadata to different groups
       according to dose level, species and sacrifice time
    """
    array_meta_df = pd.read_csv(config.array_metadata, header=0, index_col=False)
    del array_meta_df['ORGAN_ID']

    drug_label = get_drug_data()

    # remove data with drugs that are not associated a damage information
    indexing = []
    count = 0
    for ind, row in array_meta_df.iterrows():
        count += 1
        if row['COMPOUND_NAME'].strip() in drug_label:
            indexing.append(True)
        else:
            indexing.append(False)
    array_meta_df = array_meta_df[indexing]
    
    # rat invivo single
    hours = [3, 6, 9, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    for level in dose_levels:
        for hr in hours:
            df = array_meta_df[(array_meta_df['SPECIES']=='Rat') & \
                    (array_meta_df['TEST_TYPE']=='in vivo') & \
                    (array_meta_df['DOSE_LEVEL']=='%s'%level) & \
                    (array_meta_df['SACRIFICE_PERIOD']=='%d hr'%hr)]
            DILI_numbers = {}
            DILI_classes = {}
            for ind, row in df.iterrows():
                DILI_numbers[ind] = drug_label[row['COMPOUND_NAME']]['number']
                DILI_classes[ind] = drug_label[row['COMPOUND_NAME']]['class']
            df['DILI_Number'] = pd.Series(DILI_numbers)
            df['DILI_Class'] = pd.Series(DILI_classes)
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invivo_single_%s_%dhr.csv" % (level, hr))
            df.to_csv(fpath, header=True, index=False)

    # rat invivo repeat
    days = [4, 8, 15, 29]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    for level in dose_levels:
        for day in days:
            df = array_meta_df[(array_meta_df['SPECIES']=='Rat') & \
                    (array_meta_df['TEST_TYPE']=='in vivo') & \
                    (array_meta_df['DOSE_LEVEL']=='%s'%level) & \
                    (array_meta_df['SACRIFICE_PERIOD']=='%d day'%day)]
            DILI_numbers = {}
            DILI_classes = {}
            for ind, row in df.iterrows():
                DILI_numbers[ind] = drug_label[row['COMPOUND_NAME']]['number']
                DILI_classes[ind] = drug_label[row['COMPOUND_NAME']]['class']
            df['DILI_Number'] = pd.Series(DILI_numbers)
            df['DILI_Class'] = pd.Series(DILI_classes)
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invivo_repeat_%s_%dday.csv" % (level, day))
            df.to_csv(fpath, header=True, index=False)

    # rat invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    for level in dose_levels:
        for hr in hours:
            df = array_meta_df[(array_meta_df['SPECIES']=='Rat') & \
                    (array_meta_df['TEST_TYPE']=='in vitro') & \
                    (array_meta_df['DOSE_LEVEL']=='%s'%level) & \
                    (array_meta_df['SACRIFICE_PERIOD']=='%d hr'%hr)]
            DILI_numbers = {}
            DILI_classes = {}
            for ind, row in df.iterrows():
                DILI_numbers[ind] = drug_label[row['COMPOUND_NAME']]['number']
                DILI_classes[ind] = drug_label[row['COMPOUND_NAME']]['class']
            df['DILI_Number'] = pd.Series(DILI_numbers)
            df['DILI_Class'] = pd.Series(DILI_classes)
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invitro_%s_%dhr.csv" % (level, hr))
            df.to_csv(fpath, header=True, index=False)
    
    # human invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    for level in dose_levels:
        for hr in hours:
            df = array_meta_df[(array_meta_df['SPECIES']=='Human') & \
                    (array_meta_df['TEST_TYPE']=='in vitro') & \
                    (array_meta_df['DOSE_LEVEL']=='%s'%level) & \
                    (array_meta_df['SACRIFICE_PERIOD']=='%d hr'%hr)]
            print df.shape
            DILI_numbers = {}
            DILI_classes = {}
            for ind, row in df.iterrows():
                DILI_numbers[ind] = drug_label[row['COMPOUND_NAME']]['number']
                DILI_classes[ind] = drug_label[row['COMPOUND_NAME']]['class']
            df['DILI_Number'] = pd.Series(DILI_numbers)
            df['DILI_Class'] = pd.Series(DILI_classes)
            fpath = os.path.join(config.meta_folder, "array_meta_human_invitro_%s_%dhr.csv" % (level, hr))
            df.to_csv(fpath, header=True, index=False)

def reformat_exprs():
    """Reformat expression data for different groups and write to corresponding files, 
       the last column is the class label
    """

    # rat invivo single
    hours = [3, 6, 9, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.rat_invivo_single_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invivo_single_sample)
    gene_names = read_gene_names(config.rat_invivo_single_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    for level in dose_levels:
        for hr in hours:
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invivo_single_%s_%dhr.csv" % (level, hr))
            meta_df = pd.read_csv(fpath, header=0, index_col=0)
            new_expr_df = pd.DataFrame(columns=expr_df.columns)
            drug_label = {}
            for ind, row in expr_df.iterrows():
                sname = int(ind.split("_")[0].lstrip("00"))
                if sname in meta_df.index:
                    new_expr_df = new_expr_df.append(row, verify_integrity=True)
                    drug_label[ind] = meta_df['DILI_Class'][sname]
                else:
                    continue
                    #print "Rat invivo: cannot find %s" % sname
            new_expr_df['DILI_Class'] = pd.Series(drug_label)
            fpath = os.path.join(config.rat_invivo_single_folder, "expr_%s_%dhr.csv" % (level, hr))
            new_expr_df.to_csv(fpath, header=True, index=True)

    # rat invivo repeat
    days = [4, 8, 15, 29]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.rat_invivo_repeat_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invivo_repeat_sample)
    gene_names = read_gene_names(config.rat_invivo_repeat_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    for level in dose_levels:
        for day in days:
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invivo_repeat_%s_%dday.csv" % (level, day))
            meta_df = pd.read_csv(fpath, header=0, index_col=0)
            new_expr_df = pd.DataFrame(columns=expr_df.columns)
            drug_label = {}
            for ind, row in expr_df.iterrows():
                sname = int(ind.split("_")[0].lstrip("00"))
                if sname in meta_df.index:
                    new_expr_df = new_expr_df.append(row, verify_integrity=True)
                    drug_label[ind] = meta_df['DILI_Class'][sname]
                else:
                    continue
                    #print "Rat invivo repeat: cannot find %s" % sname
            new_expr_df['DILI_Class'] = pd.Series(drug_label)
            fpath = os.path.join(config.rat_invivo_repeat_folder, "expr_%s_%dday.csv" % (level, day))
            new_expr_df.to_csv(fpath, header=True, index=True)

    # rat invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.rat_invitro_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invitro_sample)
    gene_names = read_gene_names(config.rat_invitro_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    for level in dose_levels:
        for hr in hours:
            fpath = os.path.join(config.meta_folder, "array_meta_rat_invitro_%s_%dhr.csv" % (level, hr))
            meta_df = pd.read_csv(fpath, header=0, index_col=0)
            new_expr_df = pd.DataFrame(columns=expr_df.columns)
            drug_label = {}
            for ind, row in expr_df.iterrows():
                sname = int(ind.split("_")[0].lstrip("00"))
                if sname in meta_df.index:
                    new_expr_df = new_expr_df.append(row, verify_integrity=True)
                    drug_label[ind] = meta_df['DILI_Class'][sname]
                else:
                    continue
                    #print "Rat invitro: cannot find %s" % sname
            new_expr_df['DILI_Class'] = pd.Series(drug_label)
            fpath = os.path.join(config.rat_invitro_folder, "expr_%s_%dhr.csv" % (level, hr))
            new_expr_df.to_csv(fpath, header=True, index=True)
    
    # human invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.human_invitro_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.human_invitro_sample)
    gene_names = read_gene_names(config.human_invitro_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    
    for level in dose_levels:
        for hr in hours:
            fpath = os.path.join(config.meta_folder, "array_meta_human_invitro_%s_%dhr.csv" % (level, hr))
            meta_df = pd.read_csv(fpath, header=0, index_col=0)
            new_expr_df = pd.DataFrame(columns=expr_df.columns)
            drug_label = {}
            for ind, row in expr_df.iterrows():
                sname = int(ind.split("_")[0].lstrip("00"))
                if sname in list(meta_df.index):
                    new_expr_df = new_expr_df.append(row, verify_integrity=True)
                    drug_label[ind] = meta_df['DILI_Class'][sname]
                else:
                    continue
                    #print "Human invitro: cannot find %s" % sname
                    
            new_expr_df['DILI_Class'] = pd.Series(drug_label)
            fpath = os.path.join(config.human_invitro_folder, "expr_%s_%dhr.csv" % (level, hr))
            new_expr_df.to_csv(fpath, header=True, index=True)

def reformat_pathology_data():
    """Pathology data is only for rat invivo
    """
    expr_df = pd.read_csv(config.rat_invivo_single_expression, header=None, index_col=False)
    pathology_df = pd.read_csv(config.pathology_fpath, header=0, index_col=False)

    sample_ids = {}
    for ind, row in pathology_df.iterrows():
        sid = row['CEL']
        if not sid.startswith("No"):
            sample_ids[sid] = 1

    findings = ["Necrosis", "Hypertrophy", "Microgranuloma", "Cellular infiltration", "Change"]
    for finding in findings:
        count = 0
        new_pathology_df = pd.DataFrame(columns=pathology_df.columns)
        for ind, row in pathology_df.iterrows():
            if type(row['Finding']) == float:
                continue
            arr = row['Finding'].split(",")
            arr = [f.strip() for f in arr]
            #if (finding in arr) and \
            #row["Dose Level"]!='Control' and (not row['CEL'].startswith("No")):
            if (finding in arr) and row["Time"].endswith("hr") and row["Dose Level"]!='Control':
                row['CEL'] = row['CEL'].strip("(CEL)").strip()
                new_pathology_df = new_pathology_df.append(row, verify_integrity=True)
                count += 1
        new_pathology_df.to_csv("../data/pathology/%s.csv" % finding, header=True, index=False)
        print "%s, %d" % (finding, count)

def generate_invitro_pathology():
    array_meta_df = pd.read_csv(config.array_metadata, header=0, index_col=False)
    rat_invitro_meta_df = array_meta_df[(array_meta_df['SPECIES']=='Rat') & 
                                       (array_meta_df['TEST_TYPE']=='in vitro') &
                                       (array_meta_df['DOSE_LEVEL']!='Control')]

    findings = ["Necrosis", "Hypertrophy", "Microgranuloma", "Cellular infiltration", "Change"]
    for finding in findings:
        pathology_df = pd.read_csv("../data/pathology/%s.csv" % finding, header=0, index_col=False)
        info = {}
        for ind, row in pathology_df.iterrows():
            drug = row['Name']
            level = row['Dose Level']
            time = row['Time']

            if time.startswith("9") or time.startswith("6"):
                time = 8
            elif time.startswith("3"):
                time = 2
            elif time.startswith("24"):
                time = 24
            
            if drug in info:
                if time in info[drug]:
                    info[drug][time][level] = 1
                else:
                    info[drug][time] = {}
                    info[drug][time][level] = 1
            else:
                info[drug] = {}
                info[drug][time] = {}
                info[drug][time][level] = 1
        
        fh = open("../data/invitro_pathology/%s" % finding, 'w')
        for ind, row in rat_invitro_meta_df.iterrows():
            drug = row['COMPOUND_NAME']
            level = row['DOSE_LEVEL']
            time = row['SACRIFICE_PERIOD']
            if time.startswith("8"):
                time = 8
            elif time.startswith("2"):
                time = 2
            elif time.startswith("24"):
                time = 24

            if (drug in info) and (time in info[drug]) and (level in info[drug][time]):
                fh.write("%s\n" % row['BARCODE'])
        fh.close()

def validate_data():
    # rat invivo single
    hours = [3, 6, 9, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']

    expr_df = pd.read_csv(config.rat_invivo_single_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invivo_single_sample)
    gene_names = read_gene_names(config.rat_invivo_single_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names

    for level in dose_levels:
        for hr in hours:
            expr_fpath = os.path.join(config.rat_invivo_single_folder, "expr_%s_%dhr.csv" % (level, hr))
            df = pd.read_csv(expr_fpath, header=0, index_col=0)
            del df['DILI_Class']
            for index in df.index:
                expr_row = df.ix[index].values
                orig_expr_row = expr_df.ix[index].values
                if len(expr_row) != len(orig_expr_row):
                    print "format error"
                for i in range(len(expr_row)):
                    if expr_row[i] != orig_expr_row[i]:
                        print "format error"

    # rat invivo repeat
    days = [4, 8, 15, 29]
    dose_levels = ['Control', 'Low', 'Middle', 'High']

    expr_df = pd.read_csv(config.rat_invivo_repeat_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invivo_repeat_sample)
    gene_names = read_gene_names(config.rat_invivo_repeat_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names

    for level in dose_levels:
        for day in days:
            expr_fpath = os.path.join(config.rat_invivo_repeat_folder, "expr_%s_%dday.csv" % (level, day))
            df = pd.read_csv(expr_fpath, header=0, index_col=0)
            del df['DILI_Class']
            for index in df.index:
                expr_row = df.ix[index].values
                orig_expr_row = expr_df.ix[index].values
                if len(expr_row) != len(orig_expr_row):
                    print "format error"
                for i in range(len(expr_row)):
                    if expr_row[i] != orig_expr_row[i]:
                        print "format error"

    # rat invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.rat_invitro_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.rat_invitro_sample)
    gene_names = read_gene_names(config.rat_invitro_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    for level in dose_levels:
        for hr in hours:
            expr_fpath = os.path.join(config.rat_invitro_folder, "expr_%s_%dhr.csv" % (level, hr))
            df = pd.read_csv(expr_fpath, header=0, index_col=0)
            del df['DILI_Class']
            for index in df.index:
                expr_row = df.ix[index].values
                orig_expr_row = expr_df.ix[index].values
                if len(expr_row) != len(orig_expr_row):
                    print "format error"
                for i in range(len(expr_row)):
                    if expr_row[i] != orig_expr_row[i]:
                        print "format error"

    # human invitro
    hours = [2, 8, 24]
    dose_levels = ['Control', 'Low', 'Middle', 'High']
    expr_df = pd.read_csv(config.human_invitro_expression, header=None, index_col=False)
    sample_names = read_sample_names(config.human_invitro_sample)
    gene_names = read_gene_names(config.human_invitro_gene)
    expr_df.columns = gene_names
    expr_df.index = sample_names
    for level in dose_levels:
        for hr in hours:
            expr_fpath = os.path.join(config.human_invitro_folder, "expr_%s_%dhr.csv" % (level, hr))
            df = pd.read_csv(expr_fpath, header=0, index_col=0)
            del df['DILI_Class']
            for index in df.index:
                expr_row = df.ix[index].values
                orig_expr_row = expr_df.ix[index].values
                if len(expr_row) != len(orig_expr_row):
                    print "format error"
                for i in range(len(expr_row)):
                    if expr_row[i] != orig_expr_row[i]:
                        print "format error"

def get_rat_human_mapping():
    mapping = {}
    with open(config.orthology_mapping_fpath) as fh:
        fh.readline()
        for line in fh:
            line = line.strip()
            arr = line.split("\t")
            if len(arr) != 4:
                print "Warning: Format not consistant - %s" % line
            if arr[2] == 'NA':
                continue
            rat = arr[0]
            human = arr[1]
            gene_name = arr[2].lower()
            if rat in mapping:
                print "Warning: %s already exists, duplicated key" % rat
            mapping[rat] = human
    return mapping


def main():
    #split_metadata()

    #reformat_exprs()

    #reformat_pathology_data()

    #validate_data()

    generate_invitro_pathology()

if __name__ == "__main__":
    main()

