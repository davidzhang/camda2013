import os

data_folder = "../data"

meta_folder = os.path.join(data_folder, "tgp_info")
rat_invivo_single_folder = os.path.join(data_folder, "rat_invivo_single_collapsed_farms")
rat_invivo_repeat_folder = os.path.join(data_folder, "rat_invivo_repeat_collapsed_farms")
rat_invitro_folder = os.path.join(data_folder, "rat_in_vitro_collapsed_farms")
human_invitro_folder = os.path.join(data_folder, "human_in_vitro_collapsed_farms")

# rat and human ortholog gene mapping file
orthology_mapping_fpath = os.path.join(data_folder, "rat_human_orhtology.txt")

# Meta data
array_metadata = os.path.join(meta_folder, "Array Metadata.fixedchars.unix.csv")
drug_fpath = os.path.join(meta_folder, "Drug Information.csv")
pathology_fpath = os.path.join(meta_folder, "Pathology data.csv")

# Pathology data Expresion
pathology_expr_folder = os.path.join(data_folder, "pathology_expr")

# rat invivo single
rat_invivo_single_expression = os.path.join(rat_invivo_single_folder, "exprs_rat_invivo_single_collapsed.csv")
rat_invivo_single_gene = os.path.join(rat_invivo_single_folder, "geneNames_rat_invivo_single_collapsed.csv")
rat_invivo_single_sample = os.path.join(rat_invivo_single_folder, "sampleNames_rat_invivo_single_collapsed.csv")
rat_invivo_single_ini = os.path.join(rat_invivo_single_folder, "ini_rat_invivo_single_collapsed.csv")

# rat invivo repeat
rat_invivo_repeat_expression = os.path.join(rat_invivo_repeat_folder, "exprs_rat_invivo_repeat_collapsed.csv")
rat_invivo_repeat_gene = os.path.join(rat_invivo_repeat_folder, "geneNames_rat_invivo_repeat_collapsed.csv")
rat_invivo_repeat_sample = os.path.join(rat_invivo_repeat_folder, "sampleNames_rat_invivo_repeat_collapsed.csv")
rat_invivo_repeat_ini = os.path.join(rat_invivo_repeat_folder, "ini_rat_invivo_repeat_collapsed.csv")

# rat invitro
rat_invitro_expression = os.path.join(rat_invitro_folder, "exprs_rat_invitro_collapsed.csv")
rat_invitro_gene = os.path.join(rat_invitro_folder, "geneNames_rat_invitro_collapsed.csv")
rat_invitro_sample = os.path.join(rat_invitro_folder, "sampleNames_rat_invitro_collapsed.csv")
rat_invitro_ini = os.path.join(rat_invitro_folder, "ini_rat_invitro_collapsed.csv")

# human invitro
human_invitro_expression = os.path.join(human_invitro_folder, "exprs_human_invitro_collapsed.csv")
human_invitro_gene = os.path.join(human_invitro_folder, "geneNames_human_invitro_collapsed.csv")
human_invitro_sample = os.path.join(human_invitro_folder, "sampleNames_human_invitro_collapsed.csv")
human_invitro_ini = os.path.join(human_invitro_folder, "ini_human_invitro_collapsed.csv")

