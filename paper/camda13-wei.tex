\documentclass[12pt]{article}

% amsmath package, useful for mathematical formulas
\usepackage{amsmath}
% amssymb package, useful for mathematical symbols
\usepackage{amssymb}

% graphicx package, useful for including eps and pdf graphics
% include graphics with the command \includegraphics
\usepackage{graphicx}
\usepackage{subfig} 
% cite package, to clean up citations in the main text. Do not remove.
\usepackage{cite}

\usepackage{url}

\usepackage{color} 

% Use doublespacing - comment out for single spacing
%\usepackage{setspace} 
%\doublespacing


% Text layout
\topmargin 0.0cm
\oddsidemargin 0.5cm
\evensidemargin 0.5cm
\textwidth 16cm 
\textheight 21cm

% Bold the 'Figure #' in the caption and separate it with a period
% Captions will be left justified
\usepackage[labelfont=bf,labelsep=period,justification=raggedright]{caption}

% Use the bibtex style
\bibliographystyle{camda13}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother


% Leave date blank
\date{}

\pagestyle{myheadings}
%% ** EDIT HERE **


%% ** EDIT HERE **
%% PLEASE INCLUDE ALL MACROS BELOW

%% END MACROS SECTION

\begin{document}

% Title must be 150 characters or less
\begin{flushleft}
{\Large
\textbf{Assess Genomic Biomarkers of Toxicity in Drug Development}
}
% Insert Author names, affiliations and corresponding author email.
\\
Wei Zhang,
Scott Emrich and
Erliang Zeng$^{\ast}$
\\
\bf Department of Computer Science and Engineering, University of Notre Dame, Notre Dame, IN, USA
%\\
%\bf{2} Author2 Dept/Program/Center, Institution Name, City, State, Country
%\\
%\bf{3} Author3 Dept/Program/Center, Institution Name, City, State, Country
\\
$\ast$ E-mail: ezeng@nd.edu
\end{flushleft}


\section*{Introduction}
The risk assessment especially toxicity evaluation is a critical step in drug development. Typical strategy is to use animal models and in vitro experiments as surrogates for human studies in pre-clinical tests in the early stage of drug development. Such toxicity assessment is usually conducted  based on conventional indicators such as pathology and clinical chemistry data. Although this risk assessment strategy is widely used in drug discovery, many challenges remain. For example, around 40\% of drug-induced liver injury (DILI) cases are not detected in the pre-clinical studies using the conventional indicators. In addition, the agreement between studies on animal models and human clinical trials is poor. 

Nowadays, the advances in biotechnology including high-throughput microarray and next-generation sequencing technologies have leveraged the research of risk assessment to a new era, so-called toxicogenomics era. The goal of toxicogenomics is to aid in risk assessment using genomic biomarkers. In this paper, we attempted to address the problems such as if the animal studies can be replaced by in vitro assays and whether or not we can predict the liver injury in humans using animal studies, using toxicogenomics data set provided by the Japanese toxicogenomics project~\cite{Uehara2010}, a CAMDA 2013 challenge. The hypothesis is that genomic biomarkers will be more sensitive than conventional markers in detecting hepatotoxicity signals, thus would benefit the solution of disparity  in the filed of toxicology. 

We first explored the possibility of replacing the animal model with in vitro assay coupled with toxicogenomics. Previous studies addressed this problem using the agreement of differentially expressed gene lists from in vivo and in vitro data, and found poor agreement between the two~\cite{Pessiot2013}. Pessiot {\it et al.} then proposed to evaluate the in vivo-in vitro agreement using Gene Set Enrichment Analysis (GSEA) on collapsed probesets~\cite{Subramanian2005,Pessiot2013}, which has shown success in improving such agreement. In this paper, we proposed to use an alternative yet more successful strategy to evaluate to what extent animal studies can be replaced by the in vitro assays. Instead of comparing features (for example, differentially expressed genes) resulting from in vivo and in vitro experiments, our approach evaluates the biological consequences of those features. In this paper, the biological consequences of differentially expressed genes include  pathological measurements and DILI. Our method compares the powers of gene features from in vivo and in vitro data on prediction those pathological measurements and DILI levels. The hypothesis is that the processes that cause the pathology and DILI effect are complicated and may involve many factors; and the in vivo and in vitro data sets may share many common characteristics, they could also capture different biological aspects respectively. Thus focusing on the gene level comparison solely would not reveal the intrinsic relations between in vivo and in vitro data sets. Genes and proteins form a large network of interactions in living organisms. Such interaction network is the basis of biological metabolic pathway cascades. Drug toxicity effect is the results of the perturbations of biological metabolic pathway cascades. Such perturbations could happen at any level and could be induced by several key players in the cascades. The in vivo and in vitro assays could capture different or the same perturbations that cause the same pathological changes and same drug toxicity effect. Our method focuses on the analysis of pathological data and ultimate drug toxicity effect, thus provides more fair comparison between in vivo and vitro assays. Because the pathological data is available for in vivo assays only, we assume that drug will cause similar pathological results in in vitro experiments as it does for in vivo assays. For DILI data, we assume drug has similar liver injury effect in humans and rats. 

We next address the problem of predicting the liver injury in humans using toxicogenomics data from animals. We explore the possibility of predicting the DILI potential in humans using the in vitro data from rat primary hepatocytes or human primary hepatocytes. Again, the assumption is that drug will cause similar liver injury in humans and rats. 


\section*{Materials and Methods}
The Japanese toxicogenomics project provided close to 20,000 pre-processed Affymetrix microarrays used to measure the 
effects of 131 drugs on the liver~\cite{Uehara2010}. These include rat in vivo data with two experiment designs (single and repeated) and in vitro data of both rat 
and human (using rat and human hepatocytes). Various  drug dose levels and sacrifice time after treatments were applied in the experiment design. We used 
FARMS-summarized and collapsed gene expression values as described previously~\cite{clevertexploiting, pessiot2013impact} in our modeling 
and analysis. 

Among the 131 drugs, 101 were associated with one of the following categories: Most DILI concern, less DILI concern and no DILI concern. We 
considered DILI prediction as a binary classification problem. Unlike previous work\cite{pessiot2013impact} that used two classes of Most DILI
against Less DILI or No DILI, we used control microarray data as one class and microarray treated with Most DILI concern and Less DILI concern 
drugs as the other class, since we found it is difficult to differentiate Most DILI from Less DILI from gene expression data. Each microarray data is 
represented by 12,088 genes (rats) or 18988 genes (humans).

The challenge provides 5569 summaries of the rat liver pathology reports as well as full blood workups were provided, 

As to classification model, we applied Random Forest~\cite{breiman2001}, which is an ensemble approach based on the aggregation of a set of decision 
trees, where each tree is grown from a bootstrap sample (sampling with replacement) of the original data. Average over all of the predictions from 
the individual trees is considered as the final predicted value. In addition to achieving competing prediction accuracy compared to the 
state-of-the-art machine learning methods, Random Forest also could cope with high dimensional data and has good model interpretability while 
incorporating variable selection inside the learning process.

Source codes and more results for the analysis are available at \url{https://bitbucket.org/davidzhang/camda2013}.

% Results and Discussion can be combined.
\section*{Results}
We first explored influences of different experiment designs (dose and sacrifice time) on the prediction of DILI classes. Figure~\ref{Invitro-DILI-roc} demonstrates ROC 
curves for the classification of DILI categories using rat in vitro data sets of different combinations of dose level (low, middle and high) and temporal point (3, 6, 9 and 24 
hour after treatment). The ROC curves are average results using 5-fold cross validation. Although various combinations of dose  and time point have different sets of differentially
expressed genes, they all have good and similar discriminative power in classifying samples of damaged and non-damaged. This observation suggests that the time information and does level are not critical factors in assessing drug toxicity in these dat sets. This conclusion is consistent with findings as reported by Pessiot {\it et al.} ~\cite{Pessiot2013}.

\begin{figure*}[!t]
\centering
\subfloat[Low dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_low.pdf}}\hfil
\subfloat[Middle dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_middle.pdf}}
\subfloat[High dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_high.pdf}}
\caption{ROC curve in classifying DILI using Rat In vitro data of different conditions}
\label{Invitro-DILI-roc}
\end{figure*}

To evaluate the possibility of replacing animal in vivo study with in vitro assay, we built the classifier on available rat in vitro data and then used such 
classifier to predict rat in vivo data, and vice versa. Figure~\ref{DILI-invivoinvitro-prediction} demonstrates the DILI classification ROC curve using 1,000 trees in 
Random Forest, respectively. This shows that using in vivo model can accurately predict in vitro data and not vise versa, although the result is promising (AUC=0.83). 
It implies toxic drugs altered different sets of genes expression values for in vivo and in vitro assay.

\begin{figure*}[!t]
\centering
\subfloat[Rat In vitro classify In vivo]{\includegraphics[width=3in]{../plots/invitro2invivo_1000tree.pdf}}\hfil
\subfloat[Rat In vitvo classify In vitro]{\includegraphics[width=3in]{../plots/invivo2invitro_100tree.pdf}}
\caption{DILI classification ROC curve using In vitro rat data to classify In vivo rat data and vise versa with 1000 trees in Random Forest}
\label{DILI-invivoinvitro-prediction}
\end{figure*}


\begin{figure*}[!t]
\centering
{\includegraphics[width=3in]{../plots/all_pathology.pdf}}
\caption{Five pathology classification ROC curve}
\label{pathology-prediction}
\end{figure*}

In summary, 

%\section*{References}
% The bibtex filename
\bibliography{camda13}


\end{document}

