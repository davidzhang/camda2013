\documentclass[12pt]{article}

% amsmath package, useful for mathematical formulas
\usepackage{amsmath}
% amssymb package, useful for mathematical symbols
\usepackage{amssymb}

% graphicx package, useful for including eps and pdf graphics
% include graphics with the command \includegraphics
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subfig} 
% cite package, to clean up citations in the main text. Do not remove.
\usepackage{cite}

\usepackage{url}

\usepackage{color} 

% Use doublespacing - comment out for single spacing
%\usepackage{setspace} 
%\doublespacing


% Text layout
\topmargin 0.0cm
\oddsidemargin 0.5cm
\evensidemargin 0.5cm
\textwidth 16cm 
\textheight 21cm

% Bold the 'Figure #' in the caption and separate it with a period
% Captions will be left justified
\usepackage[labelfont=bf,labelsep=period,justification=raggedright]{caption}

% Use the bibtex style
\bibliographystyle{camda13}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother


% Leave date blank
\date{}

\pagestyle{myheadings}
%% ** EDIT HERE **


%% ** EDIT HERE **
%% PLEASE INCLUDE ALL MACROS BELOW

%% END MACROS SECTION

\begin{document}

% Title must be 150 characters or less
\begin{flushleft}
{\Large
\textbf{Assess Genomic Biomarkers of Toxicity in Drug Development}
}
% Insert Author names, affiliations and corresponding author email.
\\
Wei Zhang,
Scott Emrich and
Erliang Zeng$^{\ast}$
\\
\bf Department of Computer Science and Engineering, University of Notre Dame, Notre Dame, IN, USA
%\\
%\bf{2} Author2 Dept/Program/Center, Institution Name, City, State, Country
%\\
%\bf{3} Author3 Dept/Program/Center, Institution Name, City, State, Country
\\
$\ast$ E-mail: ezeng@nd.edu
\end{flushleft}


\section{Introduction}
Typical risk assessment strategies use animal models and \emph{in vitro} experiments as surrogates for human studies in the early stages of drug development. Toxicity assessment is then conducted using conventional indicators such as pathology and clinical chemistry data. Although these methods are widely used, around 40\% of drug-induced liver injury (DILI) cases are not detected in the preclinical studies using these conventional indicators, and agreement between studies on animal models and human clinical trials is often poor. To overcome these issues, advances in modern ``-omics'' including high-throughput microarrays and next-generation sequencing technologies have allowed using genomic biomarkers in risk assessment. The underlying hypothesis is that genomic biomarkers will be more sensitive than conventional markers in detecting toxicity signals. 

In this paper, we predicted DILI based on microarray data sets provided by the Japanese toxicogenomics project~\cite{Uehara2010}, a CAMDA 2013 challenge. We first explored the possibility of replacing the animal model with in vitro assay coupled with toxicogenomics. Previous studies addressed this problem using the agreement of differentially expressed gene lists from \emph{in vivo} and \emph{in vitro} data, and found poor agreement between the two~\cite{Pessiot2013}. Pessiot {\it et al.} then proposed to evaluate the \emph{in vivo-in vitro} agreement using Gene Set Enrichment Analysis (GSEA) on collapsed probesets~\cite{Pessiot2013}, which has shown success in improving such agreement. Here we took an alternative approach. Instead of comparing features (for example, differentially expressed genes) resulting from \emph{in vivo} and \emph{in vitro} experiments, we evaluated biological consequences such as pathological measurements and observed DILI by comparing the power of gene features to predict these consequences. The underlying hypothesis was that the processes that cause pathology and DILI effects are complicated and may involve many factors; and although \emph{in vivo} and \emph{in vitro} data sets may share many common characteristics, they could also capture different biological information. Because drug toxicity could result from perturbations of biological metabolic pathways,  these effects could happen at any level and could be induced by several key players. We also explored the possibility of predicting the DILI potential in humans using the \emph{in vitro} data from rat primary hepatocytes or human primary hepatocytes. Our method focused on the analysis of resulting pathological data and therefore could provide a more fair comparison using downstream effects as our key. Because the pathological data is available for \emph{in vivo} assays only, we assumed that drug will cause similar pathological results in \emph{in vitro} experiments as it does for \emph{in vivo} assays. For DILI data, we assumed a drug has similar liver injury effect in humans and rats for predicting liver injury using toxicogenomics data from animals. 


\section{Materials and Methods}

We explored the possibility of predicting the DILI potential in humans using  Japanese toxicogenomics project data, which provided close to 20,000 pre-processed Affymetrix microarrays used to measure the 
effects of 131 drugs on the liver~\cite{Uehara2010}. These included rat \emph{in vivo} data with two experiment designs (single and repeated) and \emph{in vitro} data of both rat and human (using rat and human hepatocytes). Various drug dose levels and sacrifice time after treatments were applied in the experiment design. We used FARMS-summarized and collapsed gene expression values as described previously~\cite{pessiot2013impact} in modeling and analysis for this paper. 

Among the 131 drugs, 101 were associated with one of the following categories: ``Most DILI concern'', ``less DILI concern'' and ``no DILI concern''. We 
considered DILI prediction as a binary classification problem. Unlike previous work\cite{pessiot2013impact} that used two classes of ``Most DILI''
against ``Less DILI'' or ``No DILI'', we used the control microarray data as one class and the microarray data from `` Most DILI concern'' and ``Less DILI concern'' drugs as the other class because we found it is difficult to differentiate between these two labels. Each microarray data is represented by 12,088 genes (rats) or 18,988 genes (humans). 

CAMDA challenge also provided a total of 5569 summaries of the rat liver pathology reports as previously described~\cite{Uehara2010}, which makes supervised training possible to predict pathology. For each pathology finding, we ignore severity and create a dataset for that finding and use binary classification model to classify it. As in previous work\cite{shigetta2013statistical}, only the five most frequent pathology findings, for which the largest data sets were available, were evaluated: hypertrophy, necrosis, cellular infiltration, microgranuloma and cellular change.

As to the classification model used we applied Random Forest (RF)~\cite{breiman2001}, which is an ensemble approach based on the aggregation of a set of decision trees, 
where each tree is grown from a bootstrap sample (sampling with replacement) of the original data. The average over all of the predictions from the individual trees was 
considered as the final predicted value. In addition to achieving competing prediction accuracy compared to the state-of-the-art machine learning methods, Random Forest 
also could cope with high dimensional data and has good model interpretability while incorporating variable selection inside the learning process.

Source code and additional results from the analysis are available at: \url{https://bitbucket.org/davidzhang/camda2013}.

% Results and Discussion can be combined.
\section{Results}
We first explored influences of different experiment designs (dose and sacrifice time) on the prediction of DILI potential. Figure \ref{Invitro-DILI-roc} demonstrates ROC 
curves for the classification of DILI categories using rat \emph{in vitro} data sets of different combinations of dose level (low, middle and high) and time point (3, 6, 9 and 24 hour after treatment). The ROC curves are averaged results using 5-fold cross validation. Although various combinations of dose  and time points have different sets of differentially expressed genes, they all have good and similar discriminative power in classifying samples as damaged and non-damaged. This observation suggests that time information and dose level are not critical factors in assessing drug toxicity in these data, which is consistent with the prior findings of Pessiot {\it et al.} ~\cite{Pessiot2013}.

\begin{figure*}[!t]
\centering
\subfloat[Low dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_low.pdf}}\hfil
\subfloat[Middle dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_middle.pdf}}
\subfloat[High dose level]{\includegraphics[width=2in]{../plots/rat_invivo_single_high.pdf}}
\caption{ROC curves in classifying DILI using Rat \emph{in vitro} data}
\label{Invitro-DILI-roc}
\end{figure*}

\subsection{Comparisons between rat \emph{in vivo} and \emph{in vitro} studies }
To evaluate the possibility of replacing an animal \emph{in vivo} study with \emph{in vitro} assay, we built a classifier on available rat \emph{in vitro} data and then used it to predict rat \emph{in vivo} data. Figure~\ref{DILI-invivoinvitro-prediction}(a) demonstrates the ROC curve of DILI classification using 1,000 trees in the Random Forest. The result is promising (AUC=0.83). On the other hand, the RF model built on \emph{in vivo} data can perfectly predict DILI potential of \emph{in vitro} assay (AUC=1.00, results not shown). In addition to providing prediction, RF also calculates variable importance (VIM) for each feature when constructing the model. We then compared two gene lists: one list include genes in \emph{in vivo} study with VIMs not larger than 0 (referred to as \emph{in vivo} gene list), and the other list contains genes  from \emph{in vitro} assay whose VIMs are not larger than 0 (referred to as \emph{in vitro} gene list). These two gene lists were obtained according to RF VIMs based on RF models constructed on  \emph{in vivo} and \emph{in vitro} data sets separately. Figure \ref{venn} shows the {\it Venn} diagram of the comparison. Among 5,845 total important genes, only 1575 (26.88\%) genes are shared by two lists, indicating poor agreement when comparison is performed between gene features. Nevertheless, using DILI classification as an interpretation of important genes from the two lists is a better approach to compare rat \emph{in vivo} and \emph{in vitro} data sets. Since DILI is derived eventually from pathology and clinical chemistry data, thus we expect classifier using RF model for predicting pathological data also preform good. Figure \ref{pathology-prediction} demonstrates five leading pathologies classification using RF model built on rat \emph{in vivo} data (Figure \ref{pathology-prediction}(a)) and rat \emph{in vitro} data (Figure \ref{pathology-prediction}(b)) .


%Results as shown in Figure \ref{venn} confirm our hypothesis.1,571 genes were regulated in both datasets, while both of them have some unique genes regulated. 


%We hypothesize that toxic drugs altered expression of different sets of genes between the \emph{in vivo} and \emph{in vitro} assay. So we created classification models for \emph{in vivo} and \emph{in vitro} separately and compare important genes (differentially expressed genes) between them according to RF variable importance measurement. 






\subsection{Comparisons between human \emph{in vitro} and rat \emph{in vitro} data }
The results as shown in Figure~\ref{DILI-invivoinvitro-prediction}(a) demonstrate that replace animal model with \emph{in vitro} assay is possible. Furthermore, we attempted to predict DILI in humans using rat toxicogenomics data. We created an ortholog gene mapping between human genes and rat genes according to their corresponding probe-set common gene names and obtained a list of 9,947 pairs of ortholog genes. A RF classification model was constructed using rat \emph{in vitro} data and such model was then used to predict DILI potential of human \emph{in vitro} gene expression data. The result as shown in Figure \ref{DILI-invivoinvitro-prediction}(b) demonstrates that we could accurately classify human DILI (AUC=1.0) using model built on rat \emph{in vitro} data, which implies it is possible to predict the liver injury in humans using toxicogenomics data from animal \emph{in vitro} assays.

\begin{figure*}[!t]
\centering
\subfloat[Rat \emph{in vitro} classify rat \emph{in vivo}]{\includegraphics[width=2.5in]{../plots/invitro2invivo_1000tree.pdf}}\hfil
\subfloat[Rat \emph{in vivo} classify human \emph{in vitro}]{\includegraphics[width=2.5in]{../plots/rat_invitro2human_100tree.pdf}}
\caption{ROC curves of rat \emph{in vivo} DILI classification using RF model built on rat \emph{in vitro} data (a) and of human \emph{in vitro} DILI classification using RF model built on rat \emph{in vivo} data (b)}
\label{DILI-invivoinvitro-prediction}
\end{figure*}

\begin{figure*}[!t]
\centering
\subfloat[Using RF model built on rat \emph{in vivo} data]{\includegraphics[width=2.5in]{../plots/all_pathology.pdf}}\hfil
\subfloat[Using RF model built on rat \emph{in vitro} data]{\includegraphics[width=2.5in]{../plots/invitro_pathology.pdf}}
\caption{ROC curves for classifying five pathologies}
\label{pathology-prediction}
\end{figure*}

\begin{figure*}[!t]
\centering
{\includegraphics[width=2in]{../plots/invivo_invitro_venn.pdf}}
\caption{Venn diagram of common differentially expressed genes in \emph{in vivo} and \emph{in vitro} microarray}
\label{venn}
\end{figure*}

 



%\section*{References}
% The bibtex filename
\bibliography{camda13}


\end{document}

